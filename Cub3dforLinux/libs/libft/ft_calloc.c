/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 14:49:33 by alouis            #+#    #+#             */
/*   Updated: 2020/07/26 14:38:42 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void *ptr;

	if (count > 0 && size > 0)
	{
		if (!(ptr = malloc(size * count)))
			return (NULL);
		ft_bzero(ptr, count * size);
		return (ptr);
	}
	return (0);
}
