/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 12:06:00 by alouis            #+#    #+#             */
/*   Updated: 2019/11/11 15:03:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char		*t_dst;
	const unsigned char	*t_src;
	size_t				i;

	t_dst = dst;
	t_src = src;
	i = 0;
	while (i < n)
	{
		t_dst[i] = t_src[i];
		if (t_dst[i] == (unsigned char)c)
			return ((void *)&t_dst[i + 1]);
		i++;
	}
	return (0);
}
