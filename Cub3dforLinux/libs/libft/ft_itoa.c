/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/10 15:26:19 by alouis            #+#    #+#             */
/*   Updated: 2020/02/17 18:10:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	size(int n)
{
	size_t	len;

	len = 0;
	if (n == 0 || n < 0)
	{
		len++;
		n *= -1;
	}
	while (n)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

char			*ft_itoa(int n)
{
	char			*a;
	size_t			len;
	unsigned int	nb;

	nb = n;
	len = size(n);
	if (!(a = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	a[len--] = '\0';
	if (n < 0)
	{
		a[0] = '-';
		nb *= -1;
	}
	if (n >= 0)
	{
		a[len] = nb + 48;
	}
	while (nb)
	{
		a[len--] = nb % 10 + 48;
		nb = nb / 10;
	}
	return (a);
}
