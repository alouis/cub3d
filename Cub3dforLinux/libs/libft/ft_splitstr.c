/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_splitstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 11:54:48 by alouis            #+#    #+#             */
/*   Updated: 2020/08/12 15:13:20 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_charset(char c, char *charset)
{
	int i;

	i = 0;
	while (charset[i])
	{
		if (charset[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int				ft_cw(char *str, char *charset)
{
	int	i;
	int	wc;
	int	flag;

	i = 0;
	wc = 0;
	if (ft_charset(str[0], charset) == 0 && str[i])
		wc++;
	if (!(*charset) && *str)
		return (1);
	while (str[i])
	{
		flag = 0;
		while (ft_charset(str[i], charset) == 1 && str[i])
		{
			i++;
			flag = 1;
		}
		if (flag == 1 && str[i] != '\0')
			wc++;
		if (str[i] != '\0')
			i++;
	}
	return (wc);
}

char			*ft_malloc_word(char *str, char *charset)
{
	int		i;
	char	*word;

	i = 0;
	while (ft_charset(str[i], charset) == 0 && str[i])
		i++;
	word = (char *)malloc(sizeof(char) * (i + 1));
	if (!(word))
		return (NULL);
	i = 0;
	while (ft_charset(str[i], charset) == 0 && str[i])
	{
		word[i] = str[i];
		i++;
	}
	word[i] = '\0';
	return (word);
}

char			**ft_splitstr(char *str, char *charset)
{
	int		i;
	char	**tab;

	if (!(tab = (char **)malloc(sizeof(char *) * (ft_cw(str, charset) + 1))))
		return (NULL);
	i = 0;
	while (*str)
	{
		while (ft_charset(*str, charset) == 1 && *str)
			str++;
		if (ft_charset(*str, charset) == 0 && *str)
		{
			tab[i] = ft_malloc_word(str, charset);
			i++;
			while (ft_charset(*str, charset) == 0 && *str)
				str++;
		}
	}
	tab[i] = "\0";
	return (tab);
}
