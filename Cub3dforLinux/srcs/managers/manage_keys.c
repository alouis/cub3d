/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_keys.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/15 15:28:23 by alouis            #+#    #+#             */
/*   Updated: 2020/08/12 14:24:08 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	key_pressed(int key, t_img *img)
{
	if (key == KEY_A)
		img->keys = img->keys | LEFT;
	if (key == KEY_D)
		img->keys = img->keys | RIGHT;
	if (key == KEY_W || key == KEY_UP)
		img->keys = img->keys | UP;
	if (key == KEY_S || key == KEY_DOWN)
		img->keys = img->keys | DOWN;
	if (key == KEY_LEFT)
		img->keys = img->keys | ROT_LEFT;
	if (key == KEY_RIGHT)
		img->keys = img->keys | ROT_RIGHT;
	if (key == KEY_ESC && img->m_prm)
		ft_exit(img, "");
	return (1);
}

int	key_released(int key, t_img *img)
{
	if (key == KEY_A)
		img->keys = img->keys & ~LEFT;
	if (key == KEY_D)
		img->keys = img->keys & ~RIGHT;
	if (key == KEY_W || key == KEY_UP)
		img->keys = img->keys & ~UP;
	if (key == KEY_S || key == KEY_DOWN)
		img->keys = img->keys & ~DOWN;
	if (key == KEY_LEFT)
		img->keys = img->keys & ~ROT_LEFT;
	if (key == KEY_RIGHT)
		img->keys = img->keys & ~ROT_RIGHT;
	return (1);
}

int	key_manager(t_img *img)
{
	if ((img->keys & LEFT) != 0)
		move_left(img);
	if ((img->keys & RIGHT) != 0)
		move_right(img);
	if ((img->keys & UP) != 0)
		move_forward(img);
	if ((img->keys & DOWN) != 0)
		move_backward(img);
	if ((img->keys & ROT_LEFT) != 0)
		rotate_left(img);
	if ((img->keys & ROT_RIGHT) != 0)
		rotate_right(img);
	draw_labyrinth(img);
	return (1);
}
