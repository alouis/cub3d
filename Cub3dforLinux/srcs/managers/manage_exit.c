/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_exit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/15 15:24:15 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:46:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	ft_exit(t_img *img, char *str)
{
	free_map_params(img->m_prm);
	if (img->ply != NULL)
		free(img->ply);
	if (img->z_buff != NULL)
		free(img->z_buff);
	if (img->win_ptr != NULL && !img->save)
		mlx_destroy_window(img->mlx_ptr, img->win_ptr);
	if (str && ft_has_one(str, '!'))
		ft_putstr_fd(str, 1);
	exit(1);
}
