/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_free_parsing.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 17:11:08 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:46:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	init_map_params(t_cub *m_prm)
{
	m_prm->r = NULL;
	m_prm->no = NULL;
	m_prm->so = NULL;
	m_prm->we = NULL;
	m_prm->ea = NULL;
	m_prm->s = NULL;
	m_prm->spr = NULL;
	m_prm->n_spr = 0;
	m_prm->f = NULL;
	m_prm->c = NULL;
	m_prm->map = NULL;
	m_prm->x = 0;
	m_prm->y = 0;
	m_prm->cp = 0;
	m_prm->pos = NULL;
}

void	free_map_params(t_cub *m_prm)
{
	if (m_prm->r != NULL)
		free(m_prm->r);
	if (m_prm->no != NULL)
		free(m_prm->no);
	if (m_prm->so != NULL)
		free(m_prm->so);
	if (m_prm->we != NULL)
		free(m_prm->we);
	if (m_prm->ea != NULL)
		free(m_prm->ea);
	if (m_prm->s != NULL)
		free(m_prm->s);
	if (m_prm->f != NULL)
		free(m_prm->f);
	if (m_prm->c != NULL)
		free(m_prm->c);
	if (m_prm->map != NULL)
		free_map(m_prm->map);
	if (m_prm->pos != NULL)
		free(m_prm->pos);
	if (m_prm->spr != NULL)
		free(m_prm->spr);
}

void	free_map(char **map)
{
	int i;

	i = 0;
	while (map[i] != NULL && *(map[i]) != '\0')
	{
		free(map[i]);
		i++;
	}
	free(map);
}
