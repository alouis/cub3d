/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_moves_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 16:51:12 by alouis            #+#    #+#             */
/*   Updated: 2020/08/19 16:51:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	move_left(t_img *img)
{
	double x;
	double y;

	x = img->ply->pos_x - img->ply->dir_y * img->ply->speed;
	y = img->ply->pos_y - img->ply->dir_x * img->ply->speed;
	if ((int)y > 0 && (int)y < img->m_prm->y - 1 &&
		img->m_prm->map[(int)y][(int)img->ply->pos_x] != '3')
		img->ply->pos_y = y;
	if ((int)x > 0 && (int)x < img->m_prm->x - 1 &&
		img->m_prm->map[(int)img->ply->pos_y][(int)x] != '3')
		img->ply->pos_x = x;
}

void	move_right(t_img *img)
{
	double x;
	double y;

	x = img->ply->pos_x + img->ply->dir_y * img->ply->speed;
	y = img->ply->pos_y + img->ply->dir_x * img->ply->speed;
	if ((int)y > 0 && (int)y < img->m_prm->y - 1 &&
		img->m_prm->map[(int)y][(int)img->ply->pos_x] != '3')
		img->ply->pos_y = y;
	if ((int)x > 0 && (int)x < img->m_prm->x - 1 &&
		img->m_prm->map[(int)img->ply->pos_y][(int)x] != '3')
		img->ply->pos_x = x;
}

void	move_forward(t_img *img)
{
	double x;
	double y;

	x = img->ply->pos_x + img->ply->dir_x * img->ply->speed;
	y = img->ply->pos_y + img->ply->dir_y * img->ply->speed;
	if ((int)y > 0 && (int)y < img->m_prm->y - 1 &&
		img->m_prm->map[(int)y][(int)img->ply->pos_x] != '3')
		img->ply->pos_y = y;
	if ((int)x > 0 && (int)x < img->m_prm->x - 1 &&
		img->m_prm->map[(int)img->ply->pos_y][(int)x] != '3')
		img->ply->pos_x = x;
}

void	move_backward(t_img *img)
{
	double x;
	double y;

	x = img->ply->pos_x - img->ply->dir_x * img->ply->speed;
	y = img->ply->pos_y - img->ply->dir_y * img->ply->speed;
	if ((int)y > 0 && (int)y < img->m_prm->y - 1 &&
		img->m_prm->map[(int)y][(int)img->ply->pos_x] != '3')
		img->ply->pos_y = y;
	if ((int)x > 0 && (int)x < img->m_prm->x - 1 &&
		img->m_prm->map[(int)img->ply->pos_y][(int)x] != '3')
		img->ply->pos_x = x;
}
