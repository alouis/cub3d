/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_labyrinth.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/14 12:18:01 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:44:09 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	draw_texture(t_img *img, t_dda *dda, int x, int y)
{
	t_mnp	txr;
	double	step;
	double	txr_pos;

	if (dda->side == 0)
		txr = img->txr[0];
	else if (dda->side == 1)
		txr = img->txr[1];
	else if (dda->side == 2)
		txr = img->txr[2];
	else
		txr = img->txr[3];
	dda->tex_x = (int)(dda->wall_x * (double)txr.w);
	dda->tex_x = (dda->side == 3 || dda->side == 1) ? txr.w - dda->tex_x - 1 :
		dda->tex_x;
	step = 1.0 * txr.h / dda->wall_h;
	txr_pos = (dda->start - img->m_prm->r[1] / 2 + dda->wall_h / 2) * step;
	y = dda->start;
	while (y++ < dda->end)
	{
		dda->tex_y = (int)txr_pos & (txr.h - 1);
		txr_pos += step;
		img->data[y * img->m_prm->r[0] + x] =
			txr.data[txr.h * dda->tex_y + dda->tex_x];
	}
}

void	draw_ceiling(t_img *img, t_dda *dda, int x)
{
	int	j;

	j = 0;
	while (j < dda->start)
	{
		img->data[j * img->m_prm->r[0] + x] =
		img->m_prm->c[0] | (img->m_prm->c[1] << 8) |
		(img->m_prm->c[0] << 16) | 0x00 << 24;
		j++;
	}
}

void	draw_floor(t_img *img, t_dda *dda, int x)
{
	int	j;

	j = dda->end;
	while (j < img->m_prm->r[1])
	{
		img->data[j * img->m_prm->r[0] + x] =
		img->m_prm->f[0] | (img->m_prm->f[1] << 8) |
		(img->m_prm->f[0] << 16) | 0x00 << 24;
		j++;
	}
}

void	draw_labyrinth(t_img *img)
{
	int		x;
	t_dda	*dda;

	x = 0;
	dda = &img->dda;
	while (x < img->m_prm->r[0])
	{
		init_dda(img, dda, x);
		draw_ceiling(img, dda, x);
		draw_texture(img, dda, x, dda->start);
		draw_floor(img, dda, x);
		x++;
	}
	draw_sprites(img, dda);
	mlx_put_image_to_window(img->mlx_ptr, img->win_ptr, img->img_ptr, 0, 0);
}
