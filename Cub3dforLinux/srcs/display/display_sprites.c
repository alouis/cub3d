/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_sprites.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/25 15:07:18 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 14:33:31 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	sprite_to_screen(t_img *img, t_dda *dda)
{
	int x;
	int y;
	int	d;
	int pxl;

	x = dda->spr_start.x - 1;
	while (++x < dda->spr_end.x)
	{
		dda->tex_x = (int)(256 * (x - (-dda->spr_w / 2 + dda->spr_x)) *
				img->sprite.w / dda->spr_w) / 256;
		if (dda->trsf_y > 0 && x > 0 && x < img->m_prm->r[0] &&
				dda->trsf_y < img->z_buff[x])
		{
			y = dda->spr_start.y - 1;
			while (++y < dda->spr_end.y)
			{
				d = y * 256 - img->m_prm->r[1] * 128 + dda->spr_h * 128;
				dda->tex_y = ((d * img->sprite.h) / dda->spr_h) / 256;
				pxl = img->sprite.data[dda->tex_y * img->sprite.w + dda->tex_x];
				if ((pxl & 0x00FFFFFF) != 0)
					img->data[y * img->m_prm->r[0] + x] = pxl;
			}
		}
	}
}

void	draw_sprites(t_img *img, t_dda *dda)
{
	int n;

	n = 0;
	init_sprite(img);
	while (n < img->m_prm->n_spr)
	{
		init_sprite_dda(img, dda, img->spr_order[n]);
		sprite_to_screen(img, dda);
		n++;
	}
	free_sprite(img);
}
