/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 14:32:34 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 15:03:19 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		get_map(int fd, t_cub *m_prm)
{
	int		ret;
	char	*line;
	int		i;

	ret = 1;
	line = NULL;
	i = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (!ft_has_one(line, '1') && ft_has_one_str(line, "FRCS"))
		{
			while (i < m_prm->y)
			{
				m_prm->map[i++] = dup_and_fill(line, m_prm->x, ' ');
				free(line);
				get_next_line(fd, &line);
			}
			free(line);
			m_prm->map[i] = "\0";
			return (0);
		}
		else
			free(line);
	}
	return (GETTING_MAP);
}

int		fst_lst_ln(char *line, char one, char blank)
{
	int flag;
	int i;

	i = 0;
	flag = 0;
	if (!line)
		return (MAP_ERROR);
	while (line[i])
	{
		if (line[i] == one)
		{
			i++;
			flag = 1;
		}
		else if (line[i] == blank)
			i++;
		else
			return (MAP_ERROR);
	}
	if (flag == 1)
		return (0);
	return (MAP_ERROR);
}

int		find_blank(t_cub *m_prm, int *x, int *y)
{
	while ((*y) < m_prm->y)
	{
		(*x) = 0;
		while ((*x) < m_prm->x)
		{
			if ((*x) == 0 || (*y) == 0 || ((*x) == m_prm->x - 1) ||
					((*y) == (m_prm->y - 1)))
			{
				if (!ft_search(m_prm->map[(*y)][(*x)], " \t02NSWE"))
					return (0);
			}
			else if (m_prm->map[(*y)][(*x)] == ' ' ||
					m_prm->map[(*y)][(*x)] == '\t')
				return (0);
			(*x)++;
		}
		(*y)++;
	}
	return (1);
}

void	flood_fill(t_cub *m_prm, int x, int y)
{
	if (x < 0 || x >= m_prm->x || y < 0 || y >= m_prm->y ||
			m_prm->map[y][x] == '\0')
		return ;
	if (m_prm->map[y][x] == '3' || m_prm->map[y][x] == '1')
		return ;
	m_prm->map[y][x] = '3';
	flood_fill(m_prm, x + 1, y);
	flood_fill(m_prm, x - 1, y);
	flood_fill(m_prm, x, y + 1);
	flood_fill(m_prm, x, y - 1);
}

int		snd_check_map(t_cub *m_prm)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (!(find_blank(m_prm, &x, &y)))
		flood_fill(m_prm, x, y);
	y = 0;
	while (y < m_prm->y)
	{
		x = 0;
		while (x < m_prm->x)
		{
			if (!ft_search(m_prm->map[y][x], "NSWE"))
				return (0);
			x++;
		}
		y++;
	}
	return (LEAK_MAP);
}
