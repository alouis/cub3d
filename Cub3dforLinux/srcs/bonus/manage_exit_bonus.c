/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_exit_bonus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 16:50:49 by alouis            #+#    #+#             */
/*   Updated: 2020/08/19 16:50:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	ft_exit(t_img *img, char *str)
{
	free_map_params(img->m_prm);
	if (img->ply)
		free(img->ply);
	if (img->mnp)
		free(img->mnp);
	if (img->z_buff)
		free(img->z_buff);
	if (img->win_ptr != NULL)
		mlx_destroy_window(img->mlx_ptr, img->win_ptr);
	if (img->mlx_ptr != NULL)
		free(img->mlx_ptr);
	if (str && ft_has_one(str, '!'))
		ft_putstr_fd(str, 1);
	exit(1);
}
