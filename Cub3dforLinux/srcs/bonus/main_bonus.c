/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 16:50:37 by alouis            #+#    #+#             */
/*   Updated: 2020/08/19 16:50:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	init_image(t_img *img)
{
	img->win_ptr = NULL;
	img->keys = NONE;
	img->ply = NULL;
	img->mnp = NULL;
	img->z_buff = NULL;
	img->spr_order = NULL;
	img->spr_dist = NULL;
	if (!(img->z_buff = (double *)malloc(sizeof(double) * img->m_prm->r[0])))
		ft_exit(img, "Memory allocation failed\n");
}

int		main(int argc, char **argv)
{
	t_img	img;

	if (!(img.m_prm = (t_cub *)malloc(sizeof(t_cub))))
		return (-1);
	if ((argc == 2 || argc == 3) && !(mainparsing(argv[1], img.m_prm)))
	{
		init_image(&img);
		img.save = (argc == 3) ? 1 : 0;
		init_mlx_window(&img);
		init_player(&img);
		init_textures(&img);
		init_minimap(&img);
		if (argc == 3)
			img_to_bmp(&img, argv[2]);
		draw_labyrinth(&img);
		mlx_hook(img.win_ptr, 33, 1L << 17, ft_exit, &img);
		mlx_hook(img.win_ptr, 2, 1L << 0, key_pressed, &img);
		mlx_hook(img.win_ptr, 3, 1L << 1, key_released, &img);
		mlx_loop_hook(img.mlx_ptr, key_manager, &img);
		mlx_loop(img.mlx_ptr);
	}
	if (argc == 1 || argc > 3)
		ft_putstr_fd("Exec must be followed by .cub file\n", 1);
	free(img.m_prm);
	return (-1);
}
