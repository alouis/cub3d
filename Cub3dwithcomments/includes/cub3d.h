/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/08 14:41:11 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:26:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H

# include <stdio.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <mlx.h>
# include <math.h>
# include "libft.h"

# define MAX_WIN_W 2560
# define MAX_WIN_H 1440

# define KEY_A 0
# define KEY_S 1
# define KEY_D 2
# define KEY_W 13
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define KEY_ESC 53


typedef enum	e_cp
{
	INIT, NORTH, SOUTH, WEST, EAST
}				t_cp;

typedef enum	e_pars_err
{
	NO_ERROR, MAP_ERROR, COLOR_ERROR, TEXTURE_ERROR, MALLOC_ERROR,
	GETTING_MAP, LEAK_MAP, FD_ERROR, PLAYER_ERROR
}				t_pars_err;

typedef enum	e_keys
{
	NONE = 0,
	LEFT = 0b1,
	RIGHT = 0b10,
	UP = 0b100,
	DOWN = 0b1000,
	ROT_LEFT = 0b10000,
	ROT_RIGHT = 0b100000
}				t_keys;

typedef struct	s_vector
{
	double		x;
	double		y;
}				t_vect;

typedef struct	s_cub
{
	int			*r;
	char		*no;
	char		*so;
	char		*we;
	char		*ea;
	char		*s;
	t_vect		*spr;
	int			n_spr;
	int			*f;
	int			*c;
	char		**map;
	int			y;
	int			x;
	t_cp		cp;
	int			*pos;

}				t_cub;

typedef struct	s_minimap			//struct used for textures & sprites as well
{
	void		*img;
	int			*data;
	int			bpp;
	int			sl;
	int			end;
	int			w;
	int			h;
}				t_mnp;

typedef struct	s_player
{
	double		pos_x;
	double		pos_y;
	double		dir_x;
	double		dir_y;
	double		plane_x;
	double		plane_y;
	double		speed;
	double		rot_speed;
}				t_player;

typedef struct s_dda
{
	double		delta_x;			//distance between x-sides
	double		delta_y;
	double		side_x;				//distance to the next x-side from initial pos
	double		side_y;
	double		ray_dir_x;			
	double		ray_dir_y;
	int			map_x;				//which square of the map the ray crosses
	int			map_y;
	int			step_x;				//what direction to step in x or y-direction (either +1 or -1)
	int			step_y;
	int			side;				//which side of the wall was hit
	int			wall_h;				//wall's height for 1 pixel's column
	int			start;				//where to start drawing wall on the screen 
	int			end;				//where to stop drawing wall on the screen
	double		wall_x;				//x coordinate of wall's pxl hit by ray
	int			tex_x;				//x coordinate of texture's pxl matching w/ wall's pxl hit by ray
	int			tex_y;				//y coordinate of texture's pxl matching w/ wall's pxl hit by ray
	double		trsf_x;				//depth inside the screen
	double		trsf_y;
	int			spr_x;				//x coordinate of sprite's pxl hit
	t_vect		spr_start;			//where to start drawing sprite on the screen, x for width, y for height
	t_vect		spr_end;			//where to end drawing sprite on the screen
	int			spr_w;
	int			spr_h;

}				t_dda;

typedef struct	s_img
{
	void		*mlx_ptr;
	void		*win_ptr;
	void		*img_ptr;
	int			*data;
	int			bpp;
	int			size_line;
	int			endian;
	t_keys		keys;
	t_cub		*m_prm;
	t_player	*ply;
	t_mnp		*mnp;
	t_dda		dda;
	t_mnp		txr[4];
	t_mnp		sprite;
	double		*z_buff;
	int			*spr_order;
	double		*spr_dist;
}				t_img;

typedef struct	s_bmp_header
{
	int			file_size;
	int			reserved1;
	int			reserved2;
	int			offset;
	int			infoheader_size;
	int			color_plane;
	int			compression;
	int			img_size;
	int			vertical_res;
	int			horizontal_res;
	int			n_colors;
	int			important_colors;
}				t_bh;

int				main(int argc, char **argv);
// -----Init, free & exit----
void			init_map_params(t_cub *m_prm);
void			free_map_params(t_cub *m_prm);
void			free_map(char **map);
void			init_mlx_window(t_img *img);
void			init_player(t_img *img);
void			heading_n_or_s(t_img *img);
void			heading_w_or_e(t_img *img);
void			init_textures(t_img *img);
void			init_sprite(t_img *img);
void			free_sprites_data(t_img *img);
void			ft_exit(t_img *img, char *str);

// ----PARSING----
// -----Parse & check elements----
int				parse_lmns(char *ln, t_cub *m_prm);
int				get_color(char *line, int **res, int n);
int				check_color_value(char *line, int **res);
int				get_texture(char *line, char **path);
// -----Parse, check & get map----
int				parse_map(int fd, char **line, char *file, t_cub *m_prm);
int				check_map(int fd, char **line, t_cub *m_prm);
int				fst_lst_ln(char *line, char one, char blank);
int				cardinal_pts(char *line, char *pts, t_cub *m_prm);
void			get_pov(char pov, t_cub *m_prm);
void			flood_fill(t_cub *m_prm, int x, int y);
int				find_blank(t_cub *m_prm, int *x, int *y);
int				snd_check_map(t_cub *m_prm);
int				get_map(int fd, t_cub *m_prm);
int				get_sprite(char *line, t_cub *m_prm);
// -----Mains---
int				parse_redirect(int fd, char *file, t_cub *m_prm);
void			set_error_msg(t_pars_err ret);
int				mainparsing(char *file, t_cub *m_prm);

// ----BITMAP----
void			img_to_bmp(t_img *img, char *argv);
void			draw_first_image(t_img *img);
void			set_bmp_headers(t_img *img, int file);
void			init_bmp_headers(t_img *img, t_bh *bh);
void			copy_pxl_data(t_img *img, int file);
// ----MINIMAP-----
// ----Create img & put it to window
void			minimap_main(t_img *img);
// ----Print----
void			draw_square(t_img *img, int w, int h, unsigned int color);
void			draw_player(t_img *img, int w, int h, unsigned int color);
int				print_minimap(t_img *img);
void			minimap_edges(int *img_data, int w, int h);

//----PLAYER'S MOVEMENTS----
void			move_left(t_img *img);
void			move_right(t_img *img);
void			move_forward(t_img *img);
void			move_backward(t_img *img);
void			rotate_left(t_img *img);
void			rotate_right(t_img *img);

//----CALCULS----
//----Walls----
void			init_dda(t_img *img, t_dda *dda, int x);
void			find_wall(t_img *img, t_dda *dda);
void			wall_height(t_img *img, t_dda *dda, int x);
//----Sprites----
void			sprites_dist_to_player(t_img *img);
void			sort_sprites(int *spr_order, double *spr_dist, int n);
void			init_sprite_dda(t_img *img, t_dda *dda, int n);
void			sprite_height_and_width(t_img *img, t_dda *dda);

//----DISPLAY----
//----Walls----
void			draw_labyrinth(t_img *img);
void			draw_ceiling(t_img *img, t_dda *dda, int x);
void			draw_floor(t_img *img, t_dda *dda, int x);
void			draw_texture(t_img *img, t_dda *dda, int x, int y);
//----Sprites----
void			draw_sprites(t_img *img, t_dda *dda);
void			sprite_to_screen(t_img *img, t_dda *dda);

// ----KEY HOOKS----
int				key_pressed(int key, t_img *img);
int				key_released(int key, t_img *img);
int				key_manager(t_img *img);

#endif
