/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculs_walls.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 12:29:37 by alouis            #+#    #+#             */
/*   Updated: 2020/07/31 16:04:10 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	wall_height(t_img *img, t_dda *dda, int x)
{
	double	wall_dist;

	if (dda->side == 0 || dda->side == 1)
		wall_dist = ((double)dda->map_x - img->ply->pos_x +
				(1.0 - (double)dda->step_x) / 2.0) / dda->ray_dir_x;
	else
		wall_dist = ((double)dda->map_y - img->ply->pos_y +
				(1.0 - (double)dda->step_y) / 2.0) / dda->ray_dir_y;
	img->z_buff[x] = wall_dist;
	dda->wall_h = (int)img->m_prm->r[1] / wall_dist;
	dda->start = ((-dda->wall_h / 2 + img->m_prm->r[1] / 2) < 0) ? 0 :
		-dda->wall_h / 2 + img->m_prm->r[1] / 2;
	dda->end = ((dda->wall_h / 2 + img->m_prm->r[1] / 2) >= img->m_prm->r[1]) ?
		img->m_prm->r[1] - 1 : dda->wall_h / 2 + img->m_prm->r[1] / 2;
	dda->wall_x = (dda->side < 2) ? img->ply->pos_y + wall_dist *
		dda->ray_dir_y : img->ply->pos_x + wall_dist * dda->ray_dir_x;
	dda->wall_x -= floor((dda->wall_x));
}

void	find_wall(t_img *img, t_dda *dda)
{
	int hit;

	hit = 0;
	while (!hit)
	{
		if (dda->side_x < dda->side_y)
		{
			dda->side_x += dda->delta_x;
			dda->map_x += dda->step_x;
			dda->side = (dda->step_x == 1) ? 0 : 1;
		}
		else
		{
			dda->side_y += dda->delta_y;
			dda->map_y += dda->step_y;
			dda->side = (dda->step_y == 1) ? 2 : 3;
		}
		if (img->m_prm->map[dda->map_y][dda->map_x] == '1')
			hit = 1;
	}
}

void	init_dda(t_img *img, t_dda *dda, int x)
{
	double camera_x;

	dda->map_x = (int)img->ply->pos_x;
	dda->map_y = (int)img->ply->pos_y;
	camera_x = 2.0 * (double)x / (double)img->m_prm->r[0] - 1.0;
	dda->ray_dir_x = img->ply->dir_x + img->ply->plane_x * camera_x;
	dda->ray_dir_y = img->ply->dir_y + img->ply->plane_y * camera_x;
	dda->delta_x = fabs(1.0 / dda->ray_dir_x);
	dda->delta_y = fabs(1.0 / dda->ray_dir_y);
	dda->step_x = (dda->ray_dir_x < 0) ? -1 : 1;
	dda->step_y = (dda->ray_dir_y < 0) ? -1 : 1;
	dda->side_x = (dda->ray_dir_x < 0) ?
		(img->ply->pos_x - (double)dda->map_x) * dda->delta_x :
		((double)dda->map_x + 1.0 - img->ply->pos_x) * dda->delta_x;
	dda->side_y = (dda->ray_dir_y < 0) ?
		(img->ply->pos_y - (double)dda->map_y) * dda->delta_y :
		((double)dda->map_y + 1.0 - img->ply->pos_y) * dda->delta_y;
	find_wall(img, dda);
	wall_height(img, dda, x);
}
