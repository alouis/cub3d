/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_and_calcul_sprites.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 12:28:43 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 15:37:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	sort_sprites(int *spr_order, double *spr_dist, int n)
{
	int		tmp_order;
	double	tmp_dist;
	int		i;
	int		j;

	i = 0;
	while (i < n)
	{
		j = i + 1;
		while (j < n)
		{
			if (spr_dist[i] < spr_dist[j])
			{
				tmp_order = spr_order[j];
				spr_order[j] = spr_order[i];
				spr_order[i] = tmp_order;
				tmp_dist = spr_dist[j];
				spr_dist[j] = spr_dist[i];
				spr_dist[i] = tmp_dist;
			}
			j++;
		}
		i++;
	}
}

void	sprites_dist_to_player(t_img *img)
{
	int		i;
	t_vect	*sprite;

	i = 0;
	sprite = img->m_prm->spr;
	while (i < img->m_prm->n_spr)
	{
		img->spr_order[i] = i;
		img->spr_dist[i] = ((img->ply->pos_x - sprite[i].x)
				* (img->ply->pos_x - sprite[i].x)
				+ (img->ply->pos_y - sprite[i].y)
				* (img->ply->pos_y - sprite[i].y));
		i++;
	}
	i = -1;
	sort_sprites(img->spr_order, img->spr_dist, img->m_prm->n_spr);
}

void	sprite_height_and_width(t_img *img, t_dda *dda)
{
	dda->spr_h = abs((int)(img->m_prm->r[1] / (dda->trsf_y)));
	dda->spr_start.y = -dda->spr_h / 2 + img->m_prm->r[1] / 2;
	if (dda->spr_start.y < 0)
		dda->spr_start.y = 0;
	dda->spr_end.y = dda->spr_h / 2 + img->m_prm->r[1] / 2;
	if (dda->spr_end.y >= img->m_prm->r[1])
		dda->spr_end.y = img->m_prm->r[1] - 1;
	dda->spr_w = abs((int)(img->m_prm->r[1] / (dda->trsf_y)));
	dda->spr_start.x = -dda->spr_w / 2 + dda->spr_x;
	if (dda->spr_start.x < 0)
		dda->spr_start.x = 0;
	dda->spr_end.x = dda->spr_w / 2 + dda->spr_x;
	if (dda->spr_end.x >= img->m_prm->r[0])
		dda->spr_end.x = img->m_prm->r[0] - 1;
}

void	init_sprite_dda(t_img *img, t_dda *dda, int n)
{
	double	sprite_x;
	double	sprite_y;
	double	inv_det;

	sprite_x = img->m_prm->spr[n].x - img->ply->pos_x + 0.5;
	sprite_y = img->m_prm->spr[n].y - img->ply->pos_y + 0.5;
	inv_det = 1.0 / (img->ply->plane_x * img->ply->dir_y - img->ply->dir_x *
			img->ply->plane_y);
	dda->trsf_x = inv_det * (img->ply->dir_y * sprite_x - img->ply->dir_x *
			sprite_y);
	dda->trsf_y = inv_det * (-img->ply->plane_y * sprite_x + img->ply->plane_x *
			sprite_y);
	dda->spr_x = (int)((img->m_prm->r[0] / 2) * (1 + dda->trsf_x /
				dda->trsf_y));
	sprite_height_and_width(img, dda);
}
