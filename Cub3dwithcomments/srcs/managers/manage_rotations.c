/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_rotations.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/14 10:07:31 by alouis            #+#    #+#             */
/*   Updated: 2020/07/18 14:06:10 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	rotate_left(t_img *img)
{
	double old_dir_x;
	double old_plane_x;

	old_dir_x = img->ply->dir_x;
	old_plane_x = img->ply->plane_x;
	img->ply->dir_x = img->ply->dir_x * cos(-img->ply->rot_speed) -
		img->ply->dir_y * sin(-img->ply->rot_speed);
	img->ply->dir_y = old_dir_x * sin(-img->ply->rot_speed) +
		img->ply->dir_y * cos(-img->ply->rot_speed);
	img->ply->plane_x = img->ply->plane_x * cos(-img->ply->rot_speed) -
		img->ply->plane_y * sin(-img->ply->rot_speed);
	img->ply->plane_y = old_plane_x * sin(-img->ply->rot_speed) +
		img->ply->plane_y * cos(-img->ply->rot_speed);
}

void	rotate_right(t_img *img)
{
	double old_dir_x;
	double old_plane_x;

	old_dir_x = img->ply->dir_x;
	old_plane_x = img->ply->plane_x;
	img->ply->dir_x = img->ply->dir_x * cos(img->ply->rot_speed) -
		img->ply->dir_y * sin(img->ply->rot_speed);
	img->ply->dir_y = old_dir_x * sin(img->ply->rot_speed) +
		img->ply->dir_y * cos(img->ply->rot_speed);
	img->ply->plane_x = img->ply->plane_x * cos(img->ply->rot_speed) -
		img->ply->plane_y * sin(img->ply->rot_speed);
	img->ply->plane_y = old_plane_x * sin(img->ply->rot_speed) +
		img->ply->plane_y * cos(img->ply->rot_speed);
}
