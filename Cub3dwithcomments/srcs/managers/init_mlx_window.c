/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mlx_window.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 16:05:40 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 14:19:42 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	init_mlx_window(t_img *img)
{
	img->mlx_ptr = mlx_init();
	if ((img->win_ptr = mlx_new_window(img->mlx_ptr, img->m_prm->r[0],
			img->m_prm->r[1], "Cub3d")) == NULL)
		ft_exit(img, "Creating mlx window failed.\n");
	img->img_ptr = mlx_new_image(img->mlx_ptr, img->m_prm->r[0],
			img->m_prm->r[1]);
	img->data = (int *)mlx_get_data_addr(img->img_ptr, &img->bpp,
			&img->size_line, &img->endian);
}
