/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_free_textures_and_sprites.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 14:43:24 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:35:08 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	init_textures(t_img *img)
{
	if (!(img->txr[0].img = mlx_xpm_file_to_image(img->mlx_ptr, img->m_prm->ea,
					&img->txr[0].w, &img->txr[0].h)))
		ft_exit(img, "Loading east texture failed.\n");
	img->txr[0].data = (int *)mlx_get_data_addr(img->txr[0].img,
			&img->txr[0].bpp, &img->txr[0].sl, &img->txr[0].end);
	if (!(img->txr[1].img = mlx_xpm_file_to_image(img->mlx_ptr, img->m_prm->we,
					&img->txr[1].w, &img->txr[1].h)))
		ft_exit(img, "Loading west texture failed.\n");
	img->txr[1].data = (int *)mlx_get_data_addr(img->txr[1].img,
			&img->txr[1].bpp, &img->txr[1].sl, &img->txr[1].end);
	if (!(img->txr[2].img = mlx_xpm_file_to_image(img->mlx_ptr, img->m_prm->so,
					&img->txr[2].w, &img->txr[2].h)))
		ft_exit(img, "Loading south texture failed.\n");
	img->txr[2].data = (int *)mlx_get_data_addr(img->txr[2].img,
			&img->txr[2].bpp, &img->txr[2].sl, &img->txr[2].end);
	if (!(img->txr[3].img = mlx_xpm_file_to_image(img->mlx_ptr, img->m_prm->no,
					&img->txr[3].w, &img->txr[3].h)))
		ft_exit(img, "Loading north texture failed.\n");
	img->txr[3].data = (int *)mlx_get_data_addr(img->txr[3].img,
			&img->txr[3].bpp, &img->txr[3].sl, &img->txr[3].end);
	if (!(img->z_buff = (double *)malloc(sizeof(double) * img->m_prm->r[0])))
		ft_exit(img, "Memory allocation failed\n");
}

void	init_sprite(t_img *img)
{
	if (!(img->sprite.img = mlx_xpm_file_to_image(img->mlx_ptr, img->m_prm->s,
					&img->sprite.w, &img->sprite.h)))
		ft_exit(img, "Loading sprite's texture failed.\n");
	img->sprite.data = (int *)mlx_get_data_addr(img->sprite.img,
			&img->sprite.bpp, &img->sprite.sl, &img->sprite.end);
	if (img->spr_dist)
		free(img->spr_dist);
	if (img->spr_order)
		free(img->spr_order);
	if (!(img->spr_order = (int *)malloc(sizeof(int) * img->m_prm->n_spr)))
		ft_exit(img, "Memory allocation failed.\n");
	if (!(img->spr_dist = (double *)malloc(sizeof(double) * img->m_prm->n_spr)))
		ft_exit(img, "Memory allocation failed.\n");
	sprites_dist_to_player(img);
}

void	free_sprites_data(t_img *img)
{
	if (img->z_buff)
		free(img->z_buff);
	if (img->spr_dist)
		free(img->spr_dist);
	if (img->spr_order)
		free(img->spr_order);
	ft_putstr_fd("Sprites' data freed\n", 1);
}
