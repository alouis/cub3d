/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_to_bmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 17:58:41 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 14:19:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	copy_pxl_data(t_img *img, int file)
{
	int x;
	int y;

	y = img->m_prm->r[1] - 1;
	while (y >= 0)
	{
		x = 0;
		while (x < img->m_prm->r[0])
		{
			write(file, &img->data[y * img->m_prm->r[0] + x], 4);
			x++;
		}
		y--;
	}
}

void	init_bmp_headers(t_img *img, t_bh *bh)
{
	//bmp file size = header size + infoheader size * 4 * img widht * img height
	bh->file_size = 14 + 40 + 4 * img->m_prm->r[0] * img->m_prm->r[1];
	bh->reserved1 = 0;
	bh->reserved2 = 0;
	//offset = where bmp img data (pxl data) start, after the 2 headers
	bh->offset = 54;
	//info header's data below
	bh->infoheader_size = 40;
	bh->color_plane = 1;
	bh->compression = 0;
	bh->img_size = 0;
	bh->vertical_res = 0;
	bh->horizontal_res = 0;
	bh->n_colors = 0;
	bh->important_colors = 0;
}

void	set_bmp_headers(t_img *img, int file)
{
	t_bh bh;

	init_bmp_headers(img, &bh);
	write(file, "BM", 2);
	write(file, &bh.file_size, 4);
	write(file, &bh.reserved1, 2);
	write(file, &bh.reserved2, 2);
	write(file, &bh.offset, 4);
	write(file, &bh.infoheader_size, 4);
	write(file, &img->m_prm->r[0], 4);
	write(file, &img->m_prm->r[1], 4);
	write(file, &bh.color_plane, 2);
	write(file, &img->bpp, 2);
	write(file, &bh.compression, 4);
	write(file, &bh.img_size, 4);
	write(file, &bh.vertical_res, 4);
	write(file, &bh.horizontal_res, 4);
	write(file, &bh.n_colors, 4);
	write(file, &bh.important_colors, 4);
}

void	draw_first_image(t_img *img)
{
	int		x;
	t_dda	*dda;

	x = 0;
	dda = &img->dda;
	while (x < img->m_prm->r[0])
	{
		init_dda(img, dda, x);
		draw_ceiling(img, dda, x);
		draw_texture(img, dda, x, dda->start);
		draw_floor(img, dda, x);
		// Drawing red walls
		/*	while (y < dda->end)
			{
			img->data[y * img->m_prm->r[0] + x] = 0xFF0000;
			y++;
			}*/
		x++;
	}
	draw_sprites(img, dda);
}

void	img_to_bmp(t_img *img, char *argv)
{
	int file;

	if (!ft_strcmp(argv, "--save"))
	{
		if ((file = open("first_image.bmp", O_CREAT | O_RDWR |
						O_TRUNC, S_IRWXU)) != -1)
		{
			draw_first_image(img);
			set_bmp_headers(img, file);
			copy_pxl_data(img, file);
			close(file);
			ft_exit(img, "A .bmp file was created.\n");
		}
		ft_exit(img, "Failed to create a .bmp file.\n");
	}
	ft_exit(img, "Third argument was mispelled, try --save\n");
}
