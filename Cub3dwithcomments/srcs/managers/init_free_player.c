/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_free_player.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/05 15:00:33 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 14:12:36 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	heading_n_or_s(t_img *img)
{
	if (img->m_prm->cp == NORTH)
	{
		img->ply->dir_x = 0.0;
		img->ply->dir_y = -1.0;
		img->ply->plane_x = 0.66;
		img->ply->plane_y = 0.0;
	}
	if (img->m_prm->cp == SOUTH)
	{
		img->ply->dir_x = 0.0;
		img->ply->dir_y = 1.0;
		img->ply->plane_x = -0.66;
		img->ply->plane_y = 0.0;
	}
}

void	heading_w_or_e(t_img *img)
{
	if (img->m_prm->cp == WEST)
	{
		img->ply->dir_x = -1.0;
		img->ply->dir_y = 0.0;
		img->ply->plane_x = 0.0;
		img->ply->plane_y = -0.66;
	}
	if (img->m_prm->cp == EAST)
	{
		img->ply->dir_x = 1.0;
		img->ply->dir_y = 0.0;
		img->ply->plane_x = 0.0;
		img->ply->plane_y = 0.66;
	}
}

void	init_player(t_img *img)
{
	if (!(img->ply = malloc(sizeof(t_player))))
		ft_exit(img, "Memory allocation didn't work\n");
	if (img->m_prm->cp == WEST || img->m_prm->cp == EAST)
		heading_w_or_e(img);
	if (img->m_prm->cp == NORTH || img->m_prm->cp == SOUTH)
		heading_n_or_s(img);
	img->ply->speed = 0.2;
	img->ply->rot_speed = 0.1;
	img->ply->pos_x = (double)img->m_prm->pos[0] + 0.45;
	img->ply->pos_y = (double)img->m_prm->pos[1] + 0.45;
}
