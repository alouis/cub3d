/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_exit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/15 15:24:15 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:35:39 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_exit(t_img *img, char *str)
{
	free_map_params(img->m_prm);
	if (img->ply)
	{
		free(img->ply);
		printf("Player freed\n");	
	}
	if (img->mnp)
	{
		free(img->mnp);
		printf("minimap mlx freed\n");
	}
	free_sprites_data(img);
	if (img->data)
	{
		free(img->data);
		printf("Image's data freed\n");
	}
	if (img->win_ptr != NULL)
		mlx_destroy_window(img->mlx_ptr, img->win_ptr);
	if (img->mlx_ptr != NULL)
		free(img->mlx_ptr);
	if (str)
		printf("%s\n", str);
	while (1);
	exit(1);
}
