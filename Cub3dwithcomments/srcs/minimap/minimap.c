/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/04 16:35:26 by alouis            #+#    #+#             */
/*   Updated: 2020/08/04 16:42:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	minimap_edges(int *img_data, int w, int h)
{
	int				i;
	int				j;
	unsigned int	white;

	j = 0;
	white = 0xFFFFFF;
	while (j < h)
	{
		i = 0;
		while (i < w)
		{
			if (i == 0 || (i == w - 1) || j == 0 || (j == h - 1))
				img_data[j * w + i] = white;
			i++;
		}
		j++;
	}
}

void	draw_square(t_img *img, int w, int h, unsigned int color)
{
	int i;
	int j;
	int width;

	j = h;
	width = 10;
	while (j < h + width)
	{
		i = w;
		while (i < w + width)
		{
			img->mnp->data[j * (img->m_prm->x * 10) + i] = color;
			i++;
		}
		j++;
	}
}

void	draw_player(t_img *img, int w, int h, unsigned int color)
{
	int				i;
	int				j;
	int				width;
	unsigned int	blue;

	j = h;
	width = 5;
	blue = 0x0000FF;
	while (j < h + width)
	{
		i = w;
		while (i < w + width)
		{
			img->mnp->data[j * (img->m_prm->x * 10) + i] = color;
			i++;
		}
		j++;
	}
}

int		print_minimap(t_img *img)
{
	int i;
	int j;

	j = -1;
	minimap_edges(img->mnp->data, img->m_prm->x * 10, img->m_prm->y * 10);
	while (j++ < img->m_prm->y)
	{
		i = 0;
		while (img->m_prm->map[j][i])
		{
			if (img->m_prm->map[j][i] == '1')
				draw_square(img, i * 10, j * 10, 0xFF0000);
			else if (img->m_prm->map[j][i] == '2')
				draw_square(img, i * 10, j * 10, 0x0000FF);
			else if (!(ft_search(img->m_prm->map[j][i], "NSWE")))
				draw_square(img, i * 10, j * 10, 0x00FF00);
			else
				draw_square(img, i * 10, j * 10, 0x000000);
			i++;
		}
	}
	draw_player(img, img->ply->pos_x * 10, img->ply->pos_y * 10, 0xFFFFFF);
	mlx_put_image_to_window(img->mlx_ptr, img->win_ptr, img->mnp->img, 5, 10);
	return (0);
}

void	minimap_main(t_img *img)
{
	if (!(img->mnp = malloc(sizeof(t_mnp))))
		ft_exit(img, "Memory allocation failed.\n");
	img->mnp->img = mlx_new_image(img->mlx_ptr, img->m_prm->x * 10, \
			img->m_prm->y * 10);
	img->mnp->data = (int *)mlx_get_data_addr(img->mnp->img, \
			&img->mnp->bpp, &img->mnp->sl, &img->mnp->end);
	print_minimap(img);
}
