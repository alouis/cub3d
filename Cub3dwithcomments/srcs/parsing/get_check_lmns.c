/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_check_lmns.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 17:14:14 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 12:41:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	get_color(char *line, int **res, int n)
{
	char	**num;
	int		i;

	i = 0;
	if (!line || !res || (n < 2 && n > 3))
		return (COLOR_ERROR);
	num = (n == 3) ? ft_splitstr(line, " ,\f\t\n\r\v") :
			ft_splitstr(line, " \f\t\n\r\v");
	while (ft_strncmp(num[i], "\0", 1))
		i++;
	if (i != n + 1)
	{
		free_map(num);
		return (COLOR_ERROR);
	}
	i = -1;
	if (!(*res = (int *)malloc(sizeof(int) * n)))
		return (MALLOC_ERROR);
	while (num[++i] && i < n && !ft_strnum(num[i + 1]))
		(*res)[i] = ft_atoi(num[i + 1]);
	free_map(num);
	if (i == n)
		return (0);
	return (COLOR_ERROR);
}

int	check_color_value(char *line, int **res)
{
	int i;

	i = 0;
	if (!ft_strncmp(line, "R ", 2))
	{
		if (((*res)[0] <= 0 || (*res)[0] > MAX_WIN_W) ||
				((*res)[1] <= 0 || (*res)[1] > MAX_WIN_W))
			return (COLOR_ERROR);
	}
	else if (!ft_strncmp(line, "C ", 2) || !ft_strncmp(line, "F ", 2))
	{
		while (i < 3)
		{
			if ((*res)[i] < 0 || (*res)[i] > 255)
				return (COLOR_ERROR);
			i++;
		}
	}
	return (0);
}

int	get_texture(char *line, char **path)
{
	char	**tmp;
	int		i;

	i = 1;
	tmp = ft_splitstr(line, " \f\t\n\r\v");
	if (!ft_strncmp(tmp[i], "./", 2) && !ft_strncmp(tmp[i + 1], "\0", 1))
	{
		if (!(*path = ft_strdup(tmp[i])))
		{
			free_map(tmp);
			return (TEXTURE_ERROR);
		}
	}
	free_map(tmp);
	if (!ft_file_ext(*path, ".xpm") && (i = open(*path, O_RDONLY)) >= 0 &&
		close(i) == 0)
		return (0);
	return (TEXTURE_ERROR);
}
