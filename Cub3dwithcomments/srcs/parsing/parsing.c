/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 17:15:09 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 13:46:35 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			parse_redirect(int fd, char *file, t_cub *m_prm)
{
	char	*ln;
	int		ret;
	int		err;
//	int		i;

	err = 0;
	while (err == 0 && (ret = get_next_line(fd, &ln)) > 0)
	{
		if (!ft_has_one_str(ln, "RNOSWEFC"))
			err = parse_lmns(ln, m_prm);
		else if (!ft_check_str(ln, " \t\n02NSWE"))
			err = 0;
		else if (!(err = ft_has_one(ln, '1')))
			err = parse_map(fd, &ln, file, m_prm);
		free(ln);
	}
/*	if (err == 0) //bout to get rid of this
	{
		printf("\ntexture's data = %s\n%s\n%s\n%s\n", m_prm->no, m_prm->so, m_prm->we, m_prm->ea);
		i = 0;
		printf("\n");
		while (i < m_prm->y)
			printf("   after ffa : %s\n", m_prm->map[i++]);
		printf("\n");
	}*/
	return (err);
}

int			parse_lmns(char *ln, t_cub *m_prm)
{
	int		err;

	err = 0;
	if (!ft_strncmp(ln, "R ", 2) && !(err = get_color(ln, &m_prm->r, 2)))
		err = check_color_value(ln, &m_prm->r);
	else if (!ft_strncmp(ln, "NO ", 3))
		err = get_texture(ln, &m_prm->no);
	else if (!ft_strncmp(ln, "SO ", 3))
		err = get_texture(ln, &m_prm->so);
	else if (!ft_strncmp(ln, "WE ", 3))
		err = get_texture(ln, &m_prm->we);
	else if (!ft_strncmp(ln, "EA ", 3))
		err = get_texture(ln, &m_prm->ea);
	else if (!ft_strncmp(ln, "S ", 2))
		err = get_texture(ln, &m_prm->s);
	else if (!ft_strncmp(ln, "F ", 2) &&
			!(err = get_color(ln, &m_prm->f, 3)))
		err = check_color_value(ln, &m_prm->f);
	else if (!ft_strncmp(ln, "C ", 2) &&
			!(err = get_color(ln, &m_prm->c, 3)))
		err = check_color_value(ln, &m_prm->c);
	return (err);
}

int			parse_map(int fd, char **line, char *file, t_cub *m_prm)
{
	int		err;

	err = 0;
	if (!(err = check_map(fd, line, m_prm)))
	{
		close(fd);
		fd = open(file, O_RDONLY);
		if (fd == -1)
			err = FD_ERROR;
		else if (!(m_prm->map = (char **)malloc(sizeof(char *) * m_prm->y + 1)))
				return (MALLOC_ERROR);
		else if ((err = get_map(fd, m_prm)) || (err = snd_check_map(m_prm)))
				return (err);
	}
	printf("map width %d height %d\n", m_prm->x, m_prm->y);
	return (err);
}

int			mainparsing(char *file, t_cub *m_prm)
{
	int		fd;
	int		ret;

	init_map_params(m_prm);
	if (!ft_file_ext(file, ".cub"))
	{
		fd = open(file, O_RDONLY);
		if (fd < 0)
			set_error_msg(FD_ERROR);
		else if (!(ret = parse_redirect(fd, file, m_prm)))
			return (0);
		else if (ret == -1)
			set_error_msg(GETTING_MAP);
		else
			set_error_msg(ret);
		free_map_params(m_prm);
		return (-1);
	}
	free_map_params(m_prm);
	printf("Please enter informations in a .cub file.\n");
	return (-1);
}

void		set_error_msg(t_pars_err ret)
{
	if (ret == 1)
		printf("Parsing error : map is invalid.\n");
	else if (ret == 2)
		printf("Parsing error : R, C or F's color is invalid.\n");
	else if (ret == 3)
		printf("Parsing error : invalid path to the textures or sprites.\n");
	else if (ret == 4)
		printf("A malloc error occured, please try again.\n");
	else if (ret == 5)
		printf("There was a problem when stocking map's informations.\n");
	else if (ret == 6)
		printf("Your map wasn't built properly and is now flooded. \
					Your player has drowned, sorry.\n");
	else if (ret == 7)
		printf("Fd error. Couldn't open the file, sorry\n");
	else if (ret == 8)
		printf("Parsing error : there's a problem with your player.\n");
	return ;
}
