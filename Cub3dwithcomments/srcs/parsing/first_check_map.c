/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_check_map.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 19:46:43 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 13:33:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

// Norme's good except for C++ commments
int		fst_lst_ln(char *line, char one, char blank)
{
	int flag;
	int i;

	i = 0;
	flag = 0;
	if (!line)
		return (MAP_ERROR);
	while (line[i])
	{
		if (line[i] == one)
		{
			i++;
			flag = 1;
		}
		else if (line[i] == blank)
			i++;
		else
			return (MAP_ERROR);
	}
	if (flag == 1)
		return (0);
	return (MAP_ERROR);
}

// Check if there's only one cardinal point in the line and if no other has been found before.
int		cardinal_pts(char *line, char *pts, t_cub *m_prm)
{
	int j;

	if (!line || !pts)
		return (-1);
	while (*line)
	{
		j = 0;
		while (pts[j])
		{
			if (*line == pts[j] && m_prm->cp != 0)
				return (PLAYER_ERROR);
			else if (*line == pts[j] && m_prm->cp == 0)
			{
				get_pov(*line, m_prm);
				j++;
			}
			else
				j++;
		}
		line++;
	}
	return (0);
}

// Store pov in struc m_prm.
void	get_pov(char cp, t_cub *m_prm)
{
	if (cp == 'N')
		m_prm->cp = NORTH;
	if (cp == 'S')
		m_prm->cp = SOUTH;
	if (cp == 'W')
		m_prm->cp = WEST;
	if (cp == 'E')
		m_prm->cp = EAST;
}

// Store position of the player.
int		get_pos(char *line, t_cub *m_prm)
{
	int i;

	i = 0;
	if (!(m_prm->pos = (int *)malloc(sizeof(int) * 2)))
		return (-1);
	while (line[i] != 'N' && line[i] != 'S' && line[i] != 'W' && line[i] != 'E')
		i++;
	m_prm->pos[0] = i;
	m_prm->pos[1] = m_prm->y;
	return (0);
}

// First check > only "012NSWE ", one (and only one) "NSWE". Store pov, pos & number of lines & columns.
int		check_map(int fd, char **ln, t_cub *m_prm)
{
	int ret;
	int i;

	i = 0;
	ret = 1;
	while (!ft_check_str(*ln, "012NSWE \t") &&
			!cardinal_pts(*ln, "NSWE", m_prm) && ret > 0)
	{
		m_prm->x = (m_prm->x < len_from_end(*ln, ' ')) ?
			len_from_end(*ln, ' ') : m_prm->x;
		if (m_prm->cp != 0 && m_prm->pos == 0 && get_pos(*ln, m_prm))
			return (PLAYER_ERROR);
		if (get_sprite(*ln, m_prm))
			return (MALLOC_ERROR);
		free(*ln);
		ret = get_next_line(fd, ln);
		m_prm->y++;
		while (ft_has_one(*ln, '1') == -1 && ret > 0)
		{
			if (ft_check_str(*ln, "02NSWE \t\n") == -1)
				return (MAP_ERROR);
			free(*ln);
			ret = get_next_line(fd, ln);
		}
	}
	if (m_prm->cp == 0)
		return (PLAYER_ERROR);
	return (0);
}
