/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/14 11:42:04 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 14:23:55 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	main(int argc, char **argv)
{
	t_img	img;

	if (!(img.m_prm = malloc(sizeof(t_cub))))
		return (-1);
	if ((argc == 2 || argc == 3) && !(mainparsing(argv[1], img.m_prm)))
	{
		init_mlx_window(&img);
		init_player(&img);
		img.keys = NONE;
		init_textures(&img);
		if (argc == 3)
			img_to_bmp(&img, argv[2]);
		draw_labyrinth(&img);
		minimap_main(&img);
		mlx_hook(img.win_ptr, 2, 0, key_pressed, &img);
		mlx_hook(img.win_ptr, 3, 0, key_released, &img);
		mlx_loop_hook(img.mlx_ptr, key_manager, &img);
		mlx_loop(img.mlx_ptr);
	}
	return (-1);
}
