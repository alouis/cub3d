/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_hex.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/17 18:23:49 by alouis            #+#    #+#             */
/*   Updated: 2020/08/12 16:04:58 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static size_t	size(unsigned long int n)
{
	size_t len;

	len = 0;
	if (n == 0)
		len++;
	while (n)
	{
		len++;
		n = n / 16;
	}
	return (len);
}

char			*ft_convert_hex(unsigned int n)
{
	char				base[17] = "0123456789abcdef";
	char				*a;
	unsigned long int	tmp;
	size_t				len;

	len = size(n);
	if (!(a = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	a[len--] = '\0';
	if (n == 0)
		a[len] = 48;
	while (n > 0)
	{
		tmp = n % 16;
		n = n / 16;
		a[len--] = base[tmp];
	}
	return (a);
}
