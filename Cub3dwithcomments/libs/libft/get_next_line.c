/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/23 15:26:09 by alouis            #+#    #+#             */
/*   Updated: 2020/08/14 13:59:36 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		check_str(char *str, char c)
{
	int i;

	i = 0;
	if (str == NULL)
		return (0);
	while (str[i] != '\0')
	{
		if (str[i] == c)
			return (1);
		i++;
	}
	return (0);
}

char	*free_str(char *str)
{
	if (str != NULL)
	{
		free(str);
		str = NULL;
	}
	return (str);
}

int		ft_read(char **dst, int fd)
{
	int		ret;
	char	*buff;

	ret = 0;
	if (!(buff = (char *)malloc(sizeof(char) * BUFFER_SIZE + 1)))
		return (0);
	while (check_str(*dst, '\n') == 0 && (ret = read(fd, buff, BUFFER_SIZE)) > 0)
	{
		buff[ret] = '\0';
		if (!(*dst = ft_join(*dst, buff)))
		{
			buff = free_str(buff);
			return (0);
		}
	}
	if (ret == -1)
	{
		buff = free_str(buff);
		return (-1);
	}
	buff = free_str(buff);
	return (1);
}

int		get_line(char ***line, char **dst)
{
	int result;

	result = 0;
	**line = free_str(**line);
	if (!(**line = ft_dup(*dst)))
	{
		**line = free_str(**line);
		*dst = free_str(*dst);
		return (-1);
	}
	if (!(*dst = ft_sub(*dst)))
	{
		**line = free_str(**line);
		*dst = free_str(*dst);
		return (-1);
	}
	return (1);
}

int		get_next_line(int fd, char **line)
{
	static char	*dst = NULL;
	int			ret;

	ret = 0;
	if (fd < 0 || !line || BUFFER_SIZE <= 0 || !(*line = ft_cal(1, 1)))
		return (-1);
	if (dst != NULL && check_str(dst, '\n'))
		return (get_line(&line, &dst));
	ret = ft_read(&dst, fd);
	if (check_str(dst, '\n') == 0 && ret > 0)
	{
		*line = free_str(*line);
		if (!(*line = ft_dup(dst)))
		{
			*line = free_str(*line);
			dst = free_str(dst);
			return (-1);
		}
		dst = free_str(dst);
		return (0);
	}
	if (ret > 0)
		return (get_line(&line, &dst));
	return (-1);
}
