/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dup_and_fill.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 15:14:34 by alouis            #+#    #+#             */
/*   Updated: 2020/08/12 16:04:04 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*dup_and_fill(char *dst, int len, char c)
{
	int				i;
	char			*cpy;

	i = 0;
	if (!(cpy = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	while (dst[i] && i < len)
	{
		cpy[i] = dst[i];
		i++;
	}
	while (i < len)
		cpy[i++] = c;
	cpy[len] = '\0';
	return (cpy);
}
