/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/27 15:56:55 by alouis            #+#    #+#             */
/*   Updated: 2020/08/13 13:34:11 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *ptr, size_t old_size, size_t new_size)
{
	size_t	len;
	char	*tmp;
	char	*res;

	tmp = ptr;
	len = -1;
	if (!(res = malloc(new_size)))
		return (NULL);
	while (++len < old_size)
		res[len] = tmp[len];
	while (len < new_size)
	{
		res[len] = 0;
		len++;
	}
	if (ptr != NULL)
		free(ptr);
	return ((void *)res);
}
