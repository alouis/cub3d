/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_has_one.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 14:45:06 by alouis            #+#    #+#             */
/*   Updated: 2020/03/11 13:37:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_has_one(char *str, char c)
{
	int i;

	i = 0;
	if (!str)
		return (-1);
	while (str[i])
	{
		if (str[i] == c)
			return (0);
		i++;
	}
	return (-1);
}

int	ft_seearch(char c, char *hay)
{
	int i;

	i = 0;
	while (hay[i])
	{
		if (hay[i] == c)
			return (0);
		i++;
	}
	return (-1);
}

int	ft_has_one_str(char *hay, char *needle)
{
	int i;

	i = 0;
	if (!hay || !needle)
		return (-1);
	while (hay[i])
	{
		if (!ft_seearch(hay[i], needle))
			return (0);
		i++;
	}
	return (-1);
}
